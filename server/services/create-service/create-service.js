const express = require('express');
const router = express.Router();
const Product = require('../model/product');

router.get('/', async (req, res) => {
  const product = await Product.find();
  res.render('index', {
    product
  });
});

router.post('/add', async (req, res, next) => {
  const product = new Product(req.body);
  await product.save();
  res.redirect('/');
});